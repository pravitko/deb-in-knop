#!/bin/bash

echo
echo "Stahnuti DebianChroot.tbz2 z webu a rozbaleni do /home adresare"
echo 

CHR_DIR="DebianChroot"

# download DebianChroot.tbz2 from website
wget http://binaries.nanowatt.cz/binstuff/DebianChroot.tbz2

# put disp & disp1 to hosts
sed -i 's/Microknoppix/Microknoppix\ disp\ disp1\ /' /etc/hosts

# in directory /home/knoppix do this :

sudo tar -xvjpf ~/$CHR_DIR.tbz2  -C /home

echo "Zkusime prekopirovat soubor "chroot-inside.sh" do chrootu a tam jej potreba jej spustit"
sudo cp ~/deb-in-knop/chroot-inside.sh ~/../$CHR_DIR

echo ""
echo "Nyni uz jen:"
echo "sudo bash /home/$CHR_DIR/chr /home/$CHR_DIR"
echo 
echo "Na flashce to potrva cca 20min, nez se vse rozbali a nastavi"
echo

# ...and jump inside the chroot
sudo bash /home/$CHR_DIR/chr /home/$CHR_DIR