#!/bin/bash
# prepare sandbox inside of chroot

# set /etc/hosts that apache can run without errors (FQDN mass)
sed -i s/debian/disp1/g /etc/hosts

# set hostname
hostname disp1

# tunning of /tmp that mysql can run & owner of the man pages
chmod 1777 /tmp -v

chown -R man:root /var/cache/man

# at mame cerstve baliky
apt-get update

# basing apps in chroot
# chroot has bad time and smtp is needed by dispatching web

aptitude -y install locales-all 

DEBIAN_FRONTEND=noninteractive aptitude -y install postfix ntpdate lynx

ntpdate-debian ; date

aptitude -y install apache2 ; apache2ctl stop

aptitude -y install libapache2-mod-php5 libapache2-mod-fcgid \
libapache2-mod-php5 \
php5 php5-cli php5-cgi php5-mysql php5-gd php5-curl

DEBIAN_FRONTEND=noninteractive aptitude -y install mysql-server

# empty root mysql pass ? FAK

# switch to root, FAK
mkdir -p /var/log/disp
chmod 777 /var/log/disp

# copy dispatching to the right place, FAK
cd ; 
tar -xvzpf /root/dispecink/var-www/dispecink.tgz -C /var/www
mv /var/www/dispecink /var/www/disp

# config for apache2
cp dispecink/etc-apache2/sites-available/disp.conf /etc/apache2/sites-available

# config tunning
# 2.radka None -> All
sed -i '2s/AllowOverride\ None/AllowOverride\ All/' /etc/apache2/sites-available/disp.conf
# 3.radka Order
sed -i '3s/Order\ Deny,Allow/Order\ Allow,Deny/' /etc/apache2/sites-available/disp.conf
# 4.radka Allow
sed -i '4s/Deny\ from\ all/Allow\ from\ All/'  /etc/apache2/sites-available/disp.conf

# echo "zakomentujeme FCGIWrapper"
sed -i \
  "s/FCGIWrapper/#FCGIWrapper/g" \
  /etc/apache2/sites-available/disp.conf

# disable default site, enable dispecink site, DONE
a2ensite disp.conf
a2enmod rewrite

# clean apache logs, FAK
echo "" > /var/log/apache2/access.log
echo "" > /var/log/apache2/error.log

# restart it
apache2ctl restart

# clean archive directory from *.deb files
#rm /var/cache/apt/archives/*.deb

# script /root/crt_user.sh zlobil, tak jsem to vyresil jinak. Zde jsem moc trpelivy nebyl, priznavam.
DB_NAME="disp1"
DB_USER="phappuser"
DB_PASS="no12problem"

# nejdrive db smazneme, jelikoz zaciname znovu
mysql -u root -e "DROP DATABASE $DB_NAME"
mysql -u root -e "show databases"
echo "$DB_NAME : bychom videt nemeli"
mysql -u root -e "CREATE DATABASE $DB_NAME"
mysql -u root -e "CREATE USER '$DB_USER'@'localhost' IDENTIFIED BY '$DB_PASS'"
mysql -u root -e "GRANT ALL PRIVILEGES ON * . * TO '$DB_USER'@'localhost'"
mysql -u root -e "FLUSH PRIVILEGES"
mysql -u root -e "SHOW DATABASES"
echo
echo "Databaze $DB_NAME uz je videt"

# mysql udaje nastavuji sedem (Martinuv script)
cp -p /var/www/disp/app/config/_database.neon /var/www/disp/app/config/database.neon
sed -i \
  -e "s/dsn:.*/dsn: \'mysql:host=127.0.0.1;dbname=$DB_NAME\'/" \
  -e "s/user:.*/user: \'$DB_USER\'/" \
  -e "s/password:.*/password: \'$DB_PASS\'/" \
  /var/www/disp/app/config/database.neon

# po inicializaci se muzeme prihlasit do DB s vyse uvedenymi udaji

# inializace mysql v Cernikovem webu. Naliji se tabulky v mysql
cd /var/www/disp/bin/mysql
php reset.php

# nastaveni .htaccess + rewrite. Misto "RewriteBase /"  dame "RewriteBase /disp/www/"
sed -i 's/RewriteBase\ \//RewriteBase\ \/disp\/www\//' /var/www/disp/www/.htaccess

chown -R www-data:www-data /var/www/disp
service apache2 reload

echo "Nyni je mozne rozjet prohlizec a zobrazit stranku http://localhost/disp/www/"